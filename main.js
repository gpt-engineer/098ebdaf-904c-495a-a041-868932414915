function updateTimeZones() {
  const stockholmTime = new Date().toLocaleString("en-US", { timeZone: "Europe/Stockholm" });
  document.getElementById('stockholm-time').textContent = stockholmTime;

  const newYorkTime = new Date().toLocaleString("en-US", { timeZone: "America/New_York" });
  document.getElementById('new-york-time').textContent = newYorkTime;

  const beijingTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Shanghai" });
  document.getElementById('beijing-time').textContent = beijingTime;

  const capeTownTime = new Date().toLocaleString("en-US", { timeZone: "Africa/Johannesburg" });
  document.getElementById('cape-town-time').textContent = capeTownTime;
}

// Update the time every second
setInterval(updateTimeZones, 1000);
